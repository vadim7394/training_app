import { Injectable } from "@angular/core";
import { Training } from "../modals/training";

@Injectable({
    providedIn: "root"
})
export class TrainingService {

    private items = new Array<Training>(
        {
            id: 0,
            title: 'Squats',
            description: 'Strengthens the legs',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/articles/2015/09/squat-problem-650x412-1508869909.jpg',
            videoUrl: 'https://www.youtube.com/embed/aclHkVaku9U'
        },
        {
            id: 1,
            title: 'Squats',
            description: 'Strengthens the legs',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/articles/2015/09/squat-problem-650x412-1508869909.jpg',
            videoUrl: 'https://www.youtube.com/embed/aclHkVaku9U'
        },
        {
            id: 2,
            title: 'Squats',
            description: 'Strengthens the legs',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/articles/2015/09/squat-problem-650x412-1508869909.jpg',
            videoUrl: 'https://www.youtube.com/embed/aclHkVaku9U'
        },
        {
            id: 3,
            title: 'Squats',
            description: 'Strengthens the legs',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/articles/2015/09/squat-problem-650x412-1508869909.jpg',
            videoUrl: 'https://www.youtube.com/embed/aclHkVaku9U'
        },
        {
            id: 4,
            title: 'Squats',
            description: 'Strengthens the legs',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/articles/2015/09/squat-problem-650x412-1508869909.jpg',
            videoUrl: 'https://www.youtube.com/embed/aclHkVaku9U'
        },
        {
            id: 5,
            title: 'Squats',
            description: 'Strengthens the legs',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/articles/2015/09/squat-problem-650x412-1508869909.jpg',
            videoUrl: 'https://www.youtube.com/embed/aclHkVaku9U'
        },
        {
            id: 6,
            title: 'Squats',
            description: 'Strengthens the legs',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/articles/2015/09/squat-problem-650x412-1508869909.jpg',
            videoUrl: 'https://www.youtube.com/embed/aclHkVaku9U'
        },
        {
            id: 7,
            title: 'Squats',
            description: 'Strengthens the legs',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/articles/2015/09/squat-problem-650x412-1508869909.jpg',
            videoUrl: 'https://www.youtube.com/embed/aclHkVaku9U'
        },
        {
            id: 8,
            title: 'Squats',
            description: 'Strengthens the legs',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/articles/2015/09/squat-problem-650x412-1508869909.jpg',
            videoUrl: 'https://www.youtube.com/embed/aclHkVaku9U'
        },
        {
            id: 9,
            title: 'Squats',
            description: 'Strengthens the legs',
            image: 'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/articles/2015/09/squat-problem-650x412-1508869909.jpg',
            videoUrl: 'https://www.youtube.com/embed/aclHkVaku9U'
        },
    );

    public getTrainigs(): Array<Training> {
        return this.items;
    }

    public getTraining(id: number): Training {
        return this.items.find((item) => item.id == id);
    }
}