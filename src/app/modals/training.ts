export class Training {
    constructor(
        public id: number,
        public title: string,
        public description: string,
        public image: string,
        public videoUrl: String,
    ) {
    }
}